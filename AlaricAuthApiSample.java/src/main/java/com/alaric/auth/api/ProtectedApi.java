package com.alaric.auth.api;

import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ProtectedApi.ROOT_URL)
@Validated
public class ProtectedApi {

    public static final String ROOT_URL = "/protected";

    @GetMapping(produces = {MediaType.TEXT_PLAIN_VALUE})
    public String get() {
        return "You have successfully reached the protected endpoint";
    }

}

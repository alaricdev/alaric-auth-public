package com.alaric.auth.api;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UnprotectedApi.ROOT_URL)
public class UnprotectedApi {

    public static final String ROOT_URL = "/unprotected";

    @GetMapping(produces = {MediaType.TEXT_PLAIN_VALUE})
    public String get() {
        return "You have successfully reached the unprotected endpoint";
    }

}

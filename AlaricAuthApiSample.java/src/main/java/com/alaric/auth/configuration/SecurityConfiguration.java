package com.alaric.auth.configuration;

import com.alaric.auth.api.UnprotectedApi;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.proc.DefaultJOSEObjectTypeVerifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${com.alaric.auth.api.scope}")
    private String scope;

    @Value("${spring.security.oauth2.resourceserver.jwt.jwk-set-uri}")
    private String jwtSetUri;

    public static final List<String> SWAGGER_PATH_LIST = Collections.unmodifiableList(Arrays
            .asList("/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/**",
                    "/swagger-ui/**", "/webjars/**", "/csrf"));

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requiresChannel().anyRequest().requiresInsecure().and().cors()
                .and().csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, UnprotectedApi.ROOT_URL).permitAll()
                .antMatchers(HttpMethod.GET, SWAGGER_PATH_LIST.toArray(new String[0])).permitAll()
                .antMatchers("/**").hasAuthority("SCOPE_" + scope)
                .anyRequest()
                .authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt().decoder(jwtDecoder());
    }

    private JwtDecoder jwtDecoder() {
        return NimbusJwtDecoder.withJwkSetUri(jwtSetUri)
                .jwtProcessorCustomizer(p -> p.setJWSTypeVerifier(
                        new DefaultJOSEObjectTypeVerifier<>(new JOSEObjectType("at+jwt")))).build();
    }

}

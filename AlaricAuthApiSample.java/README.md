# Prerequisites
- Ensure that the ***Java EE 8 SDK*** is installed. If not; please install the SDK from the [official page](https://www.oracle.com/java/technologies/java-ee-sdk-download.html). Please note that the SDK is required to **build** and **run** the sample application.
- Clone the repository to get the source-code.
```
git clone https://bitbucket.org/alaricdev/alaric-auth-public.git
```
- Navigate to the ***/AlaricAuthApiSample.java*** module and run the following command.
```
[repository-path]$ mvn clean install
```
# Usage

Run the ***src/main/java/com/alaric/auth/Application.java*** file.

Open the Swagger IU page with the following link http://localhost:8080/api/swagger-ui/

To test the ***/unprotected-api*** simply navigate to the Unprotected API section, click on it to unfold the content and simply trigger the API using the ***Try it out*** button. 

To test the ***/protected-api*** we need an ***Access Token***, if we try to trigger the API without the ***Access Token*** we would get an ***Authentication Exception***.

Retrieve an ***Access Token*** using the ***/AlaricOfflineAuthSample.java*** project and make sure to include the `Alaric.Subscription` value in the `scope` list in your `app.properties` file.

After getting your ***Access Token***, in the ***Swagger-UI*** press the ***Authorize*** button and provide your access token in the `Bearer {access_token}` format.

Now that we provided the ***Access Token*** we can trigger the ***/protected-api*** similar to the ***/unprotected-api***.
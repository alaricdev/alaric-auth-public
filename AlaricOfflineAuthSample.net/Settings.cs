﻿using System.Collections.Generic;

namespace AlaricOfflineAuthSample
{
    public class Settings
    {
        public string Authority { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public List<string> Scope { get; set; }
    }
}

﻿using System;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Colorful;
using IdentityModel.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sharprompt;
using Console = Colorful.Console;

namespace AlaricOfflineAuthSample
{
    internal static class Program
    {
        private static readonly HttpClient _tokenClient = new HttpClient();

        private static string _accessToken;
        private static DiscoveryCache _cache;
        private static DiscoveryDocumentResponse _disco;

        private static Settings _settings;

        private static async Task Main()
        {
            var styleSheet = CreateOptionsStyleSheet();

            PromptHeader();

            LoadSettings();

            if (!ValidateSettings())
            {
                Console.WriteLine("Terminating application!", Color.Red);
                return;
            }

            PromptSettings();

            PromptCredentials();

            if (!await CheckDisco().ConfigureAwait(false))
            {
                Console.WriteLine("Terminating application!", Color.Red);
                return;
            }

            PromptDisco();

            await Interact(styleSheet).ConfigureAwait(false);

            Console.WriteLine("Quitting...", Color.Yellow);
        }

        private static async Task Interact(StyleSheet styleSheet)
        {
            var quit = false;

            do
            {
                Console.WriteLineStyled(new string('=', 75), styleSheet);

                Console.WriteLineStyled(@"
    (R) - Retrieve a token from the Alaric Auth - Dev server
    (V) - Validate the token offline
    (U) - Update credentials
    (Q) - Quit", styleSheet);
                Console.WriteLine();

                var input = Prompt.Input<string>(
                    "Please select an option",
                    "Q",
                    new[]
                    {
                        PromptValidator.RegularExpression("[RVUQrvuq]"),
                        PromptValidator.MaxLength(1),
                        PromptValidator.Required()
                    });

                switch (input)
                {
                    case "R":
                    case "r":
                        await RetrieveToken().ConfigureAwait(false);
                        break;
                    case "V":
                    case "v":
                        ValidateToken();
                        break;
                    case "U":
                    case "u":
                        PromptCredentials();
                        break;
                    case "Q":
                    case "q":
                        quit = true;
                        break;
                    default:
                        Console.Write("Invalid option!\n", Color.Red);
                        break;
                }
            }
            while (!quit);
        }

        private static void PromptDisco()
        {
            Console.WriteLine($"Successfully fetched Discovery Document from {_settings.Authority}", Color.Green);
        }

        private static async Task<bool> CheckDisco()
        {
            _cache = new DiscoveryCache($"{_settings.Authority}");
            _disco = await _cache.GetAsync().ConfigureAwait(false);

            if (_disco == null || !string.IsNullOrWhiteSpace(_disco.Error))
            {
                Console.WriteLine("The authority does not respond or responds with an error. Please check the authority " +
                    "address your provided or try again later!", Color.Yellow);
                Console.WriteLine(_disco.Error);

                return false;
            }

            return true;
        }

        private static void PromptCredentials()
        {
            Console.WriteLine("\nPlease provide your credentials.");

            _settings.Username = Prompt.Input<string>("Username", validators: new[] { PromptValidator.Required() });
            _settings.Password = Prompt.Password("Password", validators: new[] { PromptValidator.Required() });
        }

        private static void PromptSettings()
        {
            Console.WriteLine("Settings loaded successfully!", Color.Green);

            Console.Write("Authority: ", Color.MediumSlateBlue);
            Console.WriteLine(_settings.Authority);

            Console.Write("Client ID: ", Color.MediumSlateBlue);
            Console.WriteLine(_settings.ClientId);

            Console.Write("Client Secret: ", Color.MediumSlateBlue);
            Console.WriteLine(_settings.ClientSecret);

            Console.Write("Scope(s): ", Color.MediumSlateBlue);
            Console.WriteLine(string.Join(',', _settings.Scope));
        }

        private static void PromptHeader()
        {
            Console.Clear();
            Console.WriteAscii("ALARIC AUTH", Color.MediumSlateBlue);
        }

        private static StyleSheet CreateOptionsStyleSheet()
        {
            var styleSheet = new StyleSheet(Color.White);
            styleSheet.AddStyle(@"\(([^)]+)\)", Color.MediumSlateBlue);

            return styleSheet;
        }

        private static void LoadSettings()
        {
            Console.WriteLine("Loading settings...", Color.Yellow);

            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true);

            var config = builder.Build();

            _settings = config.Get<Settings>();
        }

        private static bool ValidateSettings()
        {
            const string urlPattern = @"http(s)?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?";
            const string clientSecretPattern = "[0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12}";

            var settingsTemplate = JToken
                .Parse("{\"Authority\": \"https://www.example.com\",\"ClientId\": \"MyClientId\",\"ClientSecret\": \"00000000-0000-0000-0000-000000000000\",\"Scope\": [\"MyScope1\",\"MyScope2\"]}")
                .ToString(Formatting.Indented);

            if (_settings is null)
            {
                Console.WriteLine("The appsettings.json file cannot be found.", Color.Yellow);
                return false;
            }

            if (string.IsNullOrWhiteSpace(_settings.Authority)
                || string.IsNullOrWhiteSpace(_settings.ClientId)
                || string.IsNullOrWhiteSpace(_settings.ClientSecret)
                || _settings.Scope is null
                || _settings.Scope.Count == 0
                || _settings.Scope.Any(s => string.IsNullOrWhiteSpace(s)))
            {
                Console.WriteLine("The appsettings.json file is not properly constructed. Ensure that the appsettings.json file complies with the following template", Color.Yellow);
                Console.WriteLine(settingsTemplate, Color.White);

                return false;
            }

            if (!Regex.IsMatch(_settings.Authority, urlPattern))
            {
                Console.WriteLine("The authority URL must be a valid URL, e.g. https://www.example.com", Color.Yellow);

                return false;
            }

            if (!Regex.IsMatch(_settings.ClientSecret, clientSecretPattern))
            {
                Console.WriteLine("The Client Secret must be a valid GUID, e.g. 00000000-0000-0000-0000-000000000000", Color.Yellow);

                return false;
            }

            return true;
        }

        private static async Task RetrieveToken()
        {
            var tokenRequest = new PasswordTokenRequest
            {
                Address = _disco.TokenEndpoint,
                ClientId = _settings.ClientId,
                ClientSecret = _settings.ClientSecret,
                UserName = _settings.Username,
                Password = _settings.Password,
                Scope = string.Join(' ',_settings.Scope)
            };

            tokenRequest.Headers.Add(
                "Authorization",
                $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settings.ClientId}:{_settings.ClientSecret}"))}");

            var response = await _tokenClient.RequestPasswordTokenAsync(tokenRequest).ConfigureAwait(false);
            response.Show();

            _accessToken = response.AccessToken;
        }

        private static void ValidateToken()
        {
            if (string.IsNullOrWhiteSpace(_accessToken))
            {
                Console.WriteLine("A Token Cannot Be Found. Please, first retrieve a token!", Color.Red);
                return;
            }

            var keyset = _disco.KeySet;

            if (keyset == null
                || keyset.Keys == null
                || keyset.Keys.Count < 1
                || keyset.Keys[0].X5c == null
                || keyset.Keys[0].X5c.Count < 1
                || string.IsNullOrWhiteSpace(keyset.Keys[0].X5c[0]))
            {
                Console.WriteLine("Cannot Fetch the Public Key!", Color.Red);
                return;
            }

            var publicKey = keyset.Keys[0].X5c[0];

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidAudiences = _settings.Scope,                
                ValidateIssuer = true,
                ValidIssuer =$"{_settings.Authority}",
                ValidateActor = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new X509SecurityKey(new X509Certificate2(Convert.FromBase64String(publicKey)))
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            IdentityModelEventSource.ShowPII = true;

            try
            {
                var principal = tokenHandler.ValidateToken(_accessToken, tokenValidationParameters, out var rawValidatedToken);
                var securityToken = (JwtSecurityToken)rawValidatedToken;

                Console.WriteLine("The Token Is Valid!", Color.MediumSlateBlue);
            }
            catch (Exception ex)
            {
                Console.WriteLine("The Token Is Invalid!", Color.Red);
                Console.WriteLine(ex.Message, Color.Red);
            }
        }

        public static string ReadPassword()
        {
            string password = "";
            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.Escape:
                        return string.Empty;
                    case ConsoleKey.Enter:
                        return password;
                    case ConsoleKey.Backspace:
                        if (password.Length > 0)
                        {
                            password = password.Substring(0, (password.Length - 1));
                            Console.Write("\b \b");
                        }
                        break;
                    default:
                        password += key.KeyChar;
                        Console.Write("*");
                        break;
                }
            }
        }
    }
}

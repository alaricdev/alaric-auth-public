# Prerequisites
- Ensure that the ***.NET Core 3.1.100 SDK*** is installed. If not; please install the SDK from the [official page](https://dotnet.microsoft.com/download/dotnet-core/3.1). Please note that the SDK is required to **build** and **run** the sample application. If you only intend to run the application and you already have the built binaries, it is sufficient to have the ***Desktop Runtime 3.1.0***.
- Clone the repository to get the source-code.
```
git clone https://bitbucket.org/alaricdev/alaric-auth-public.git
```
- Ensure to place a valid `appsettings.json` file or update the existing template.
- Navigate to the repository folder and run the following commands.
```
[repository-path]$ dotnet restore
[repository-path]$ dotnet run
```
# Usage

The goal of this sample application is to demonstrate;
- Connecting to the authority public end-points.
- Collecting public key(s) from the authority.
- Retrieving an access token using the Resource Owner Password Flow.
- Validating the access token with the Public Key offline.

The usage is pretty simple. When you run the application, you should see an intro:

![image](doc/aoas-01.png)

The settings are loaded from the `appsettings.json` file residing under the same folder of the executable. If you see any errors, please refer to the troubleshooting section below.

To start working with tokens, you have to provide your credentials.

![image](doc/aoas-02.png)

Then the application will try to fetch the Discovery Document from the authority. Upon receiving the document, a success message and possible options will be prompted.

![image](doc/aoas-03.png)

Options are self explanatory as;

- **R** will try to retrieve an access token for the user.
- **V** will try to validate the access token. *Please note that you first have to retrieve an access token to use this option!*
- **U** will prompt the username/password fields to update previously provided credentials.
- **Q** will quit the application.
 
## Retrieve Token

When a token is successfully retrieved, the encoded token and the decoded content will be displayed.

![image](doc/aoas-04.png)
 
# Walkthrough

Most of the application code is to enhance the console experience. In this section only token related code is explained in detail.

## Discovery Document

After initializing the console, loading and prompting settings & credentials, the applicaiton is trying to load the discovery document with `CheckDisco()`.

The discovery endpoint can be used to retrieve metadata about the authority - it returns information like the issuer name, key material, supported scopes etc. See the [spec](https://openid.net/specs/openid-connect-discovery-1_0.html) for more details.

The discovery endpoint is available via /.well-known/openid-configuration relative to the base address, e.g.:

`https://www.example.com/.well-known/openid-configuration`

In this application, the discovery document is important for 2 reasons;

- Getting the token end-point URL to fetch the access token.
- Getting the keyset to validate the access token.

The application utilizes the [IdentityModel](https://github.com/IdentityModel/IdentityModel) client library to programmatically access the discovery endpoint from .NET code. Hence the following snippet of the `CheckDisco()` method fetches the discovery document.

```
...
_cache = new DiscoveryCache($"{_settings.Authority}");
_disco = await _cache.GetAsync().ConfigureAwait(false);
...
```

Now we are able to use the metadata over the `_disco` member.

## Retrieving Token

The *IdentityModel* library has an extension for *HttpClient* named `RequestPasswordTokenAsync(PasswordTokenRequest)` to directly retrieve an access token based on the request data. So it is important to build up a valid request. The `RetrieveToken()` method encapsulates this logic.

```
private static async Task RetrieveToken()
{
    var tokenRequest = new PasswordTokenRequest
    {
        Address = _disco.TokenEndpoint,
        ClientId = _settings.ClientId,
        ClientSecret = _settings.ClientSecret,
        UserName = _settings.Username,
        Password = _settings.Password,
        Scope = string.Join(' ',_settings.Scope)
    };

    tokenRequest.Headers.Add(
        "Authorization",
        $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_settings.ClientId}:{_settings.ClientSecret}"))}");

    var response = await _tokenClient.RequestPasswordTokenAsync(tokenRequest).ConfigureAwait(false);
    response.Show();

    _accessToken = response.AccessToken;
}
```

As you can see, we are building up a token request by setting;

- The address from the discovery document - token endpoint.
- Client ID and Secret from the settings.
- Credentials from the user input. ***Note:** Even the username and password are read from the `_settings` member, in the code you will observe that these are later persisted into this member from the console input rather than loading from the `appsettings.json`.*
- Scopes from the settings.

Then we have to add an authorization header in base-64 format. Finally we request the access token.

## Validating The Token

The validation part of the code consists of a single method `ValidateToken()`.

The validation logic starts by checking the existense of the `_accessToken`. It will stop execution if there isn't any.

```
if (string.IsNullOrWhiteSpace(_accessToken))
{
    ...
    return;
}
```

Then we are reading the metadata and validating if the X.509 Certificate Chain parameter exists. You can read more about JWK - JSON Web Key specifications at [RFC-7517](https://tools.ietf.org/html/rfc7517). Through this chain we can obtain the public key as per specification.

```
var keyset = _disco.KeySet;

if (keyset == null
    || keyset.Keys == null
    || keyset.Keys.Count < 1
    || keyset.Keys[0].X5c == null
    || keyset.Keys[0].X5c.Count < 1
    || string.IsNullOrWhiteSpace(keyset.Keys[0].X5c[0]))
{
    Console.WriteLine("Cannot Fetch the Public Key!", Color.Red);
    return;
}

var publicKey = keyset.Keys[0].X5c[0];
```

Now it is time to build up our validation parameters.

```
var tokenValidationParameters = new TokenValidationParameters
{
    ValidateAudience = true,
    ValidAudiences = _settings.Scope,                
    ValidateIssuer = true,
    ValidIssuer =$"{_settings.Authority}",
    ValidateActor = false,
    ValidateIssuerSigningKey = true,
    IssuerSigningKey = new X509SecurityKey(new X509Certificate2(Convert.FromBase64String(publicKey)))
};
```

The validation parameters can be extended or reduced regarding various requirements. In our scenario, we are validating against;

- The `aud` or audience. The audience is the value **for** whom this token is issued for. Hence we are validating it against the scope(s).
- The `iss` or issuer. The issuer is the value **by** whom this token is issued from. Hence we are validating it against the authority.
- We are validating the singing security key to validate that the token is not tampered.

Finally, we are running the validation using the parameters above.

```
try
{
    var principal = tokenHandler.ValidateToken(_accessToken, tokenValidationParameters, out var rawValidatedToken);
    var securityToken = (JwtSecurityToken)rawValidatedToken;
    ...
}
catch (Exception ex)
{
    ...
}
```

The only way to catch validation errors is to use a try-catch block.
 
# Troubleshooting

**Q.** Settings are not loaded and I receive an error saying; `The appsettings.json file cannot be found.`

**A.** Ensure that the appsettings.json file exists and resides under the same folder of the executable.

---

**Q.** Settings are not loaded and I receive an error saying; `The appsettings.json file is not properly constructed.` 

**A.** Ensure that the appsettings.json file content is a valid JSON document having the provided format alongside with the error. This means:

- Having a valid JSON format
- Having the same keys
- Having non-empty values
- Having at least 1 scope

```
{
  "Authority": "https://www.example.com",
  "ClientId": "MyClientId",
  "ClientSecret": "00000000-0000-0000-0000-000000000000",
  "Scope": [
    "Application1",
    "Application2"
  ]
}
```

---

**Q.** Settings are not loaded and I receive an error saying; `The authority URL must be a valid URL, e.g. https://www.example.com` 

**A.** Ensure that the authority URL is valid in the `appsettings.json` file.

---

**Q.** Settings are not loaded and I receive an error saying; `The Client Secret must be a valid GUID, e.g. 00000000-0000-0000-0000-000000000000` 

**A.** Ensure that the client secret is valid in the `appsettings.json` file.

---

**Q.** The application has started but I cannot see the options with an error saying; `The authority does not respond or responds with an error. Please check the authority address your provided or try again later!` 

**A.** There may be several cases like;

- The authority web application is down or does not respond.
- The authority URL is wrong.
- The communication to and/or from the authority web application is blocked.

Most likely, the underlying reason will be prompted to the user.


---

**Q.** When trying to retrieve a token, I instead receive an error.

**A.** At this stage, such errors are most likely credential errors. We suggest to go with the U option and update your credentials and try again. Other than that, a connection, internal server, or such temporary errors may be occurred.



# Considerations

- The application utilizes a helper library called [IdentityModel](https://github.com/IdentityModel/IdentityModel). You may re-write the very same flow using pure HTTPClient or WebClient libraries but utilizing a helper library dramatically decreases the development effort.
- The public key is always fetched from the JWKS end-point on the authority end-points. It is suggested to cache these keys for a long time like day or week, to get rid of redundant round-trips.
- It is known that the Resource Owner Password Flow is not a suggested flow but it fits our goal as the client application is a fully trusted application and this flow is suitable for background authentication scenarios.
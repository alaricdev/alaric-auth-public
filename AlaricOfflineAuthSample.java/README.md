# Prerequisites
- Ensure that the ***Java EE 8 SDK*** is installed. If not; please install the SDK from the [official page](https://www.oracle.com/java/technologies/java-ee-sdk-download.html). Please note that the SDK is required to **build** and **run** the sample application.
- Clone the repository to get the source-code.
```
git clone https://bitbucket.org/alaricdev/alaric-auth-public.git
```
- Ensure to place a valid `app.properties` file or update the existing template.
- Credentials will not be asked if they are provided in the `app.properties` file. But they can be updated if required in the application menu.
- Navigate to the repository folder ***/AlaricOfflineAuthSample.java*** and run the following commands.
```
[repository-path]$ mvn clean compile exec:java
```
# Usage

The goal of this sample application is to demonstrate;
- Connecting to the authority public end-points.
- Collecting public key(s) from the authority.
- Retrieving an access token using the Resource Owner Password Flow.
- Validating the access token with the Public Key offline.

The usage is pretty simple. When you run the application, you should see an intro:

![image](./doc/img1.png)

The settings are loaded from the `app.properties` file residing under the same folder of the executable. If you see any errors, please refer to the troubleshooting section below.

To start working with tokens, you have to provide your credentials.

![image](./doc/img2.png)

Then the application will try to fetch the Discovery Document from the authority. Upon receiving the document, a success message and possible options will be prompted.

![image](./doc/img3.png)

Options are self explanatory as;

- **R** will try to retrieve an access token for the user.
- **V** will try to validate the access token. *Please note that you first have to retrieve an access token to use this option!*
- **U** will prompt the username/password fields to update previously provided credentials.
- **Q** will quit the application.

## Retrieve Token

When a token is successfully retrieved, the encoded token and the decoded content will be displayed.

![image](./doc/img4.png)

# Walkthrough

Most of the application code is to enhance the console experience. In this section only token related code is explained in detail.

## Discovery Document

After initializing the console, loading and prompting settings & credentials, the applicaiton is trying to load the discovery document with `CheckDisco()`.

The discovery endpoint can be used to retrieve metadata about the authority - it returns information like the issuer name, key material, supported scopes etc. See the [spec](https://openid.net/specs/openid-connect-discovery-1_0.html) for more details.

The discovery endpoint is available via /.well-known/openid-configuration relative to the base address, e.g.:

`https://www.example.com/.well-known/openid-configuration`

In this application, the discovery document is important for 2 reasons;

- Getting the token end-point URL to fetch the access token.
- Getting the keyset to validate the access token.

## Retrieving Token

The retrieving token is done by executing the `retrieveToken()` method in the `TokenUtils.java` class.

First, `"token_endpoint"` value retrieved from the discovery document then a Http POST request is send to this URL using the provided settings and credentials as headers and parameters.

Following header is prepared using the Client ID and Client Secret settings in the `Basic encodeString({client_id}:{client_secret})` format.

From the response, `access_token` is retrieved and used to create a `Token` java object that contains the access token and its header, payload and signature parts.  

## Validating The Token

The validation part of the code consists of a single method `validateToken()` in the `TokenUtils.java` class.

The validation logic starts by checking the existence of the `accessToken`. It will stop execution if there isn't any.

Then the method first decodes the payload part of the access token and checks the fields against the provided settings then creates a signing info using the header and payload part of the access token and validates using the signature verifier method.

# Troubleshooting

**Q.** Settings are not loaded and I receive an error saying; `The app.properties file cannot be found.`

**A.** Ensure that the app.properties file exists and resides under the same folder of the executable.

---

**Q.** Settings are not loaded and I receive an error saying; `The app.properties file is not properly constructed.`

**A.** Ensure that the app.properties file content is a valid application properties document having the provided format alongside with the error. This means:

- Having a valid application properties format
- Having the same keys
- Having non-empty values
- Having at least 1 scope and separated by comma if more than 1

```
authority=https://www.example.com
client_id=MyClientId
client_secret=00000000-0000-0000-0000-000000000000
scope=Application1,Application2
username=
password=
```

---

**Q.** Settings are not loaded and I receive an error saying; `The authority URL must be a valid URL, e.g. https://www.example.com`

**A.** Ensure that the authority URL is valid in the `app.properties` file.

---

**Q.** Settings are not loaded and I receive an error saying; `The Client Secret must be a valid GUID, e.g. 00000000-0000-0000-0000-000000000000`

**A.** Ensure that the client secret is valid in the `app.properties` file.

---

**Q.** The application has started but I cannot see the options with an error saying; `The authority does not respond or responds with an error. Please check the authority address your provided or try again later!`

**A.** There may be several cases like;

- The authority web application is down or does not respond.
- The authority URL is wrong.
- The communication to and/or from the authority web application is blocked.

Most likely, the underlying reason will be prompted to the user.


---

**Q.** When trying to retrieve a token, I instead receive an error.

**A.** At this stage, such errors are most likely credential errors. We suggest to go with the U option and update your credentials and try again. Other than that, a connection, internal server, or such temporary errors may be occurred.

package com.alaric.auth.util;

import static com.alaric.auth.util.Constants.ACCESS_TOKEN;
import static com.alaric.auth.util.Constants.ANSI_BLUE;
import static com.alaric.auth.util.Constants.ANSI_GREEN;
import static com.alaric.auth.util.Constants.ANSI_RED;
import static com.alaric.auth.util.Constants.ANSI_RESET;
import static com.alaric.auth.util.Constants.CHECK_DISCO_ENDPOINT;
import static com.alaric.auth.util.Constants.CLIENT_ID;
import static com.alaric.auth.util.Constants.CLIENT_SECRET;
import static com.alaric.auth.util.Constants.DOT;
import static com.alaric.auth.util.Constants.ERROR_FETCH_PUBLIC_KEY;
import static com.alaric.auth.util.Constants.ERROR_INVALID_TOKEN;
import static com.alaric.auth.util.Constants.ERROR_NOT_FOUND_TOKEN;
import static com.alaric.auth.util.Constants.ERROR_NOT_INSTANTIATED;
import static com.alaric.auth.util.Constants.ERROR_PASSWORD_TOKEN_REQUEST;
import static com.alaric.auth.util.Constants.FORWARD_SLASH;
import static com.alaric.auth.util.Constants.GRANT_TYPE;
import static com.alaric.auth.util.Constants.INFO_RETRIEVED_ACCESS_TOKEN;
import static com.alaric.auth.util.Constants.INFO_VALID_TOKEN;
import static com.alaric.auth.util.Constants.ISS;
import static com.alaric.auth.util.Constants.JWKS_URI;
import static com.alaric.auth.util.Constants.KEYS;
import static com.alaric.auth.util.Constants.KEY_E;
import static com.alaric.auth.util.Constants.KEY_N;
import static com.alaric.auth.util.Constants.NEXT_LINE;
import static com.alaric.auth.util.Constants.PASSWORD;
import static com.alaric.auth.util.Constants.PROMPT_TOKEN_RESPONSE;
import static com.alaric.auth.util.Constants.PROMPT_TOKEN_RESPONSE_DECODED;
import static com.alaric.auth.util.Constants.RSA;
import static com.alaric.auth.util.Constants.SCOPE;
import static com.alaric.auth.util.Constants.SHA256_WITH_RSA;
import static com.alaric.auth.util.Constants.TOKEN_ENDPOINT;
import static com.alaric.auth.util.Constants.USERNAME;
import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.apache.http.HttpHeaders.ACCEPT;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpHeaders.CONTENT_TYPE;
import static org.apache.http.client.config.AuthSchemes.BASIC;
import static org.apache.http.entity.ContentType.APPLICATION_FORM_URLENCODED;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;

import com.alaric.auth.domain.Settings;
import com.alaric.auth.domain.Token;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.nimbusds.jose.util.Base64URL;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.text.MessageFormat;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public final class TokenUtils {

    private TokenUtils() {
        throw new UnsupportedOperationException(ERROR_NOT_INSTANTIATED);
    }

    public static void validateToken(Token token, Settings settings, JsonObject discoJson) {
        if (token == null) {
            System.out.println(ERROR_NOT_FOUND_TOKEN);
            return;
        }

        try {
            final String payload = decodeToken(token.getPayload());
            final JsonObject payloadJson = JsonParser.parseString(payload).getAsJsonObject();

            final String issuer = payloadJson.get(ISS).getAsString();
            String authority = settings.getAuthority();
            if (authority.endsWith(FORWARD_SLASH)) {
                authority = authority.substring(0, authority.length() - 1);
            }
            if (!authority.equals(issuer)) {
                System.out.println(ERROR_INVALID_TOKEN);
                return;
            }

            final String clientId = payloadJson.get(CLIENT_ID).getAsString();
            if (!settings.getClient_id().equals(clientId)) {
                System.out.println(ERROR_INVALID_TOKEN);
                return;
            }

            final JsonArray scopeJsonArray = payloadJson.get(SCOPE).getAsJsonArray();
            for (JsonElement scopeElement : scopeJsonArray) {
                if (!settings.getScope().contains(scopeElement.getAsString())) {
                    System.out.println(ERROR_INVALID_TOKEN);
                    return;
                }
            }

            Signature verifier = Signature.getInstance(SHA256_WITH_RSA);

            final JsonObject jwksKeySet = HttpRequestUtils.get(discoJson.get(JWKS_URI).getAsString());
            if (jwksKeySet == null || !jwksKeySet.has(KEYS)) {
                System.out.println(ERROR_FETCH_PUBLIC_KEY);
                return;
            }

            final JsonArray keys = jwksKeySet.get(KEYS).getAsJsonArray();
            if (keys == null || keys.size() < 1) {
                System.out.println(ERROR_FETCH_PUBLIC_KEY);
                return;
            }

            final JsonObject firstKey = keys.get(0).getAsJsonObject();
            final BigInteger modulus = decodeKey(firstKey, KEY_N);
            final BigInteger exponent = decodeKey(firstKey, KEY_E);
            final RSAPublicKeySpec keySpec = new RSAPublicKeySpec(modulus, exponent);
            final PublicKey pub = KeyFactory.getInstance(RSA).generatePublic(keySpec);
            verifier.initVerify(pub);

            final byte[] signingInfo = token.getSigningInfo().getBytes(StandardCharsets.UTF_8);
            verifier.update(signingInfo);

            final byte[] b64DecodedSig = new com.nimbusds.jose.util.Base64(token.getSignature()).decode();
            if (verifier.verify(b64DecodedSig)) {
                System.out.println(INFO_VALID_TOKEN);
            } else {
                System.out.println(ERROR_INVALID_TOKEN);
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException
                | JsonSyntaxException e) {
            System.out.println(ERROR_INVALID_TOKEN);
        }

    }

    public static Token retrieveToken(Settings settings, JsonObject discoJson) {
        Map<String, String> headers = new HashMap<>();
        headers.put(ACCEPT, APPLICATION_JSON.getMimeType());
        final String basicAuthToken = prepareBasicAuthToken(settings.getClient_id(), settings.getClient_secret());
        headers.put(AUTHORIZATION, basicAuthToken);
        headers.put(CONTENT_TYPE, APPLICATION_FORM_URLENCODED.getMimeType());

        Map<String, String> parameters = new HashMap<>();
        final String scope = String.join(SPACE, settings.getScope());
        parameters.put(SCOPE, scope);
        parameters.put(GRANT_TYPE, PASSWORD);
        parameters.put(CLIENT_ID, settings.getClient_id());
        parameters.put(CLIENT_SECRET, settings.getClient_secret());
        parameters.put(USERNAME, settings.getUsername());
        parameters.put(PASSWORD, settings.getPassword());

        final String tokenEndpoint = discoJson.get(TOKEN_ENDPOINT).getAsString();
        final JsonObject responseJson = HttpRequestUtils.post(tokenEndpoint, headers, parameters);
        if (responseJson != null) {
            final String accessToken = responseJson.get(ACCESS_TOKEN).getAsString();
            System.out.println(INFO_RETRIEVED_ACCESS_TOKEN);
            return new Token(accessToken);
        }
        System.out.println(ERROR_PASSWORD_TOKEN_REQUEST);
        return null;
    }

    public static JsonObject retrieveDiscoJson(String authority) {
        if (StringUtils.isEmpty(authority)) {
            return null;
        }

        if (!authority.endsWith(FORWARD_SLASH)) {
            authority += FORWARD_SLASH;
        }

        return HttpRequestUtils.get(authority + CHECK_DISCO_ENDPOINT);
    }

    public static void promptToken(Token token) {
        System.out.println(PROMPT_TOKEN_RESPONSE);
        System.out.println(ANSI_RED + token.getHeader() + ANSI_RESET + DOT
                + ANSI_BLUE + token.getPayload() + ANSI_RESET + DOT
                + ANSI_GREEN + token.getSignature() + ANSI_RESET);
        System.out.println(PROMPT_TOKEN_RESPONSE_DECODED);
        System.out.println(ANSI_RED + preparePrettyJsonStr(decodeToken(token.getHeader())) + NEXT_LINE
                + ANSI_BLUE + preparePrettyJsonStr(decodeToken(token.getPayload())) + ANSI_RESET);
    }

    private static String preparePrettyJsonStr(String str) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(JsonParser.parseString(str));
    }

    private static BigInteger decodeKey(JsonObject json, String key) {
        final String value = json.get(key).getAsString();
        final byte[] nBase64URL = new Base64URL(value).decode();
        return new BigInteger(1, nBase64URL);
    }

    private static String decodeToken(String token) {
        return new Base64URL(token).decodeToString();
    }

    private static String prepareBasicAuthToken(String client_id, String client_secret) {
        final String originalInput = MessageFormat.format("{0}:{1}", client_id, client_secret);
        final String encodedInput = Base64.getEncoder().encodeToString(originalInput.getBytes());
        return MessageFormat.format("{0} {1}", BASIC, encodedInput);
    }

}

package com.alaric.auth.util;

import static com.alaric.auth.util.Constants.AMPERSAND;
import static com.alaric.auth.util.Constants.EQUALS;
import static com.alaric.auth.util.Constants.ERROR_NOT_INSTANTIATED;
import static com.alaric.auth.util.Constants.ERROR_PARAMETERS_ENCODING;
import static com.alaric.auth.util.Constants.ERROR_REQUEST_CONNECTION_FAILED;
import static com.alaric.auth.util.Constants.ERROR_REQUEST_FAILED;
import static com.alaric.auth.util.Constants.ERROR_REQUEST_MALFORMED_URL;
import static com.alaric.auth.util.Constants.ERROR_RESPONSE_EMPTY;
import static com.alaric.auth.util.Constants.ERROR_RESPONSE_PROCESS;
import static com.google.api.client.http.HttpMethods.GET;
import static com.google.api.client.http.HttpMethods.POST;
import static org.apache.commons.codec.binary.Hex.DEFAULT_CHARSET_NAME;
import static org.apache.http.HttpHeaders.CONTENT_LENGTH;
import static org.apache.http.HttpStatus.SC_OK;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

public final class HttpRequestUtils {

    private HttpRequestUtils() {
        throw new UnsupportedOperationException(ERROR_NOT_INSTANTIATED);
    }

    public static JsonObject get(String requestUrl) {
        final URL url = prepareRequestUrl(requestUrl);
        if (url == null) {
            return null;
        }

        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(GET);
            if (connection.getResponseCode() != SC_OK) {
                System.out.println(ERROR_REQUEST_FAILED);
                return null;
            }

            try (final InputStream inputStream = connection.getInputStream()) {
                return prepareResponse(inputStream);
            }
        } catch (IOException e) {
            System.out.println(ERROR_REQUEST_CONNECTION_FAILED + e.getMessage());
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static JsonObject post(String requestUrl, Map<String, String> headers, Map<String, String> parameters) {
        final URL url = prepareRequestUrl(requestUrl);
        if (url == null) {
            return null;
        }

        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(POST);

            if (MapUtils.isNotEmpty(headers)) {
                for (String key : headers.keySet()) {
                    final String value = headers.get(key);
                    connection.setRequestProperty(key, value);
                }
            }

            if (MapUtils.isNotEmpty(parameters)) {
                connection.setDoOutput(true);
                final String urlParameters = prepareParamsString(parameters);
                byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                int postDataLength = postData.length;
                connection.setRequestProperty(CONTENT_LENGTH, Integer.toString(postDataLength));
                final OutputStream outputStream = connection.getOutputStream();
                try (DataOutputStream dos = new DataOutputStream(outputStream)) {
                    dos.write(postData);
                } catch (UnsupportedEncodingException e) {
                    System.out.println(ERROR_PARAMETERS_ENCODING + e.getMessage());
                    return null;
                }
            }

            try (final InputStream inputStream = connection.getInputStream()) {
                return prepareResponse(inputStream);
            }
        } catch (IOException e) {
            System.out.println(ERROR_REQUEST_CONNECTION_FAILED + e.getMessage());
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private static URL prepareRequestUrl(String requestUrl) {
        try {
            return new URL(requestUrl);
        } catch (MalformedURLException e) {
            System.out.println(ERROR_REQUEST_MALFORMED_URL + e.getMessage());
            return null;
        }
    }

    private static String prepareParamsString(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), DEFAULT_CHARSET_NAME));
            result.append(EQUALS);
            result.append(URLEncoder.encode(entry.getValue(), DEFAULT_CHARSET_NAME));
            result.append(AMPERSAND);
        }

        String resultString = result.toString();
        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }

    private static JsonObject prepareResponse(InputStream inputStream) {
        final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        try (BufferedReader in = new BufferedReader(inputStreamReader)) {
            String inputLine;
            StringBuilder strBuilder = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                strBuilder.append(inputLine);
            }
            String result = strBuilder.toString();

            if (StringUtils.isEmpty(result)) {
                System.out.println(ERROR_RESPONSE_EMPTY);
                return null;
            }

            final JsonObject responseJson = JsonParser.parseString(result).getAsJsonObject();
            if (responseJson == null) {
                System.out.println(ERROR_RESPONSE_PROCESS);
            }
            return responseJson;
        } catch (IOException e) {
            System.out.println(ERROR_RESPONSE_PROCESS);
            return null;
        } catch (JsonSyntaxException e) {
            System.out.println(ERROR_RESPONSE_PROCESS + e.getMessage());
            return null;
        }
    }

}

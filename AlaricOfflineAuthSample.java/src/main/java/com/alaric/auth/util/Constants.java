package com.alaric.auth.util;

public final class Constants {

    private Constants() {
        throw new UnsupportedOperationException(ERROR_NOT_INSTANTIATED);
    }

    // Console colors
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_WHITE = "\u001B[37m";

    // Separators
    public static final String AMPERSAND = "&";
    public static final String COMMA = ",";
    public static final String DOT = ".";
    public static final String DOT_ESCAPE = "\\.";
    public static final String FORWARD_SLASH = "/";
    public static final String EQUALS = "=";
    public static final String NEXT_LINE = "\n";
    public static final String TAB = "\t";

    // Json properties
    public static final String AUTHORITY = "authority";
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String SCOPE = "scope";
    public static final String TOKEN_ENDPOINT = "token_endpoint";
    public static final String GRANT_TYPE = "grant_type";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String JWKS_URI = "jwks_uri";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String KEYS = "keys";
    public static final String KEY_N = "n";
    public static final String KEY_E = "e";
    public static final String ISS = "iss";

    // Token verification
    public static final String RSA = "RSA";
    public static final String SHA256_WITH_RSA = "SHA256withRSA";

    // Prompt keys
    public static final String PROMPT_AUTHORITY = ANSI_BLUE + "Authority: " + ANSI_RESET;
    public static final String PROMPT_CLIENT_ID = ANSI_BLUE + "Client ID: " + ANSI_RESET;
    public static final String PROMPT_CLIENT_SECRET = ANSI_BLUE + "Client Secret: " + ANSI_RESET;
    public static final String PROMPT_SCOPES = ANSI_BLUE + "Scope(s): " + ANSI_RESET;
    public static final String PROMPT_USERNAME = "Username: " + ANSI_RESET;
    public static final String PROMPT_PASSWORD = "Password: " + ANSI_RESET;
    public static final String TITLE_DIVIDER = createDivider(10);
    public static final String PROMPT_TOKEN_RESPONSE = NEXT_LINE + ANSI_YELLOW + TITLE_DIVIDER
            + " Token Response " + TITLE_DIVIDER + ANSI_RESET;
    public static final String PROMPT_TOKEN_RESPONSE_DECODED = NEXT_LINE + ANSI_YELLOW + TITLE_DIVIDER
            + " Token Response - Decoded " + TITLE_DIVIDER + ANSI_RESET;

    // Menu interacts
    public static final String MENU_DIVIDER = ANSI_RESET + createDivider(75);
    public static final String INTERACT_MENU = NEXT_LINE + MENU_DIVIDER
            + ANSI_BLUE + NEXT_LINE + TAB + "(R)" + ANSI_RESET + " - Retrieve a token from the Alaric Auth - Dev server"
            + ANSI_BLUE + NEXT_LINE + TAB + "(V)" + ANSI_RESET + " - Validate the token offline"
            + ANSI_BLUE + NEXT_LINE + TAB + "(U)" + ANSI_RESET + " - Update credentials"
            + ANSI_BLUE + NEXT_LINE + TAB + "(Q)" + ANSI_RESET + " - Quit" + ANSI_RESET;

    // Menu options
    public static final String OPTION_R = "R";
    public static final String OPTION_V = "V";
    public static final String OPTION_U = "U";
    public static final String OPTION_Q = "Q";

    // File paths
    public static final String APP_PROPERTIES_PATH = "app.properties";

    // Endpoint URLs
    public static final String CHECK_DISCO_ENDPOINT = ".well-known/openid-configuration";

    // Regex patterns
    public static String URL_PATTERN
            = "^(http(s)?):\\/\\/[-a-zA-Z0-9+&@#\\/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#\\/%=~_|]";
    public static String CLIENT_SECRET_PATTERN
            = "[0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12}";
    public static String MENU_OPTION_PATTERN = "[RVUQrvuq]";

    // Example templates
    public static final String SETTINGS_TEMPLATE = ANSI_RESET + "Authority=https://www.example.com" + NEXT_LINE
            + "ClientId=MyClientId" + NEXT_LINE + "ClientSecret=00000000-0000-0000-0000-000000000000" + NEXT_LINE
            + "Scope=MyScope1,MyScope2";

    // Process console logs
    public static final String INFO_LOADING_SETTINGS = ANSI_YELLOW + NEXT_LINE
            + "Loading settings..." + NEXT_LINE + ANSI_RESET;
    public static final String INFO_LOADED_SETTINGS = ANSI_GREEN + NEXT_LINE
            + "Settings loaded successfully!" + NEXT_LINE + ANSI_RESET;
    public static final String INFO_PLEASE_PROVIDE_YOUR_CREDENTIALS = ANSI_WHITE + NEXT_LINE
            + "Please provide your credentials." + NEXT_LINE + ANSI_RESET;
    public static final String INFO_FETCHING_DISCOVERY_DOCUMENT = ANSI_GREEN + NEXT_LINE
            + "Fetching Discovery Document from Authority: ";
    public static final String INFO_FETCHED_DISCOVERY_DOCUMENT = ANSI_GREEN + NEXT_LINE
            + "Successfully fetched Discovery Document from Authority:";
    public static final String INFO_PLEASE_SELECT_OPTION = NEXT_LINE + "Please select an option: " + ANSI_RESET;
    public static final String INFO_QUITTING = ANSI_YELLOW + NEXT_LINE + "Quitting..." + ANSI_RESET;
    public static final String INFO_RETRIEVED_ACCESS_TOKEN = ANSI_GREEN + NEXT_LINE
            + "Successfully retrieved Access Token" + NEXT_LINE + ANSI_RESET;
    public static final String INFO_VALID_TOKEN = ANSI_GREEN + ANSI_GREEN + NEXT_LINE
            + "Token is Valid" + NEXT_LINE + ANSI_RESET;

    // Error console logs
    public static final String ERROR_TERMINATING_APPLICATION = ANSI_RED + NEXT_LINE
            + "Terminating application!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_READING_APP_PROPERTIES_FILE = ANSI_RED + NEXT_LINE
            + "The app.properties file not found!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_NULL_APP_PROPERTIES = ANSI_RED + NEXT_LINE
            + "The app.properties cannot be null!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_MALFORMED_APP_PROPERTIES = ANSI_RED + NEXT_LINE
            + "The app.properties file is not properly constructed. "
            + "Ensure that the app.properties file complies with the following template" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_INVALID_AUTHORITY_URL = ANSI_RED + NEXT_LINE
            + "The authority URL must be a valid URL, e.g. https://www.example.com" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_INVALID_CLIENT_SECRET = ANSI_RED + NEXT_LINE
            + "The Client Secret must be a valid GUID, e.g. 00000000-0000-0000-0000-000000000000"
            + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_INVALID_USERNAME = ANSI_RED + NEXT_LINE
            + "Username cannot be empty!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_INVALID_PASSWORD = ANSI_RED + NEXT_LINE
            + "Password cannot be empty!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_RESPONSE_EMPTY = ANSI_RED + NEXT_LINE
            + "Response cannot be empty!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_RESPONSE_PROCESS = ANSI_RED + NEXT_LINE
            + "Response cannot be processed!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_PARAMETERS_ENCODING = ANSI_RED + NEXT_LINE
            + "Request parameters cannot be encoded!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_REQUEST_CONNECTION_FAILED = ANSI_RED + NEXT_LINE
            + "Request connection failed!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_REQUEST_FAILED = ANSI_RED + NEXT_LINE
            + "Request is failed!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_REQUEST_MALFORMED_URL = ANSI_RED + NEXT_LINE
            + "Request URL is malformed!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_INVALID_OPTION = ANSI_RED + NEXT_LINE
            + "Invalid option!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_PASSWORD_TOKEN_REQUEST = ANSI_RED + NEXT_LINE
            + "Retrieve token request failed!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_NOT_FOUND_TOKEN = ANSI_RED + NEXT_LINE
            + "A Token Cannot Be Found. Please, first retrieve a token!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_FETCH_PUBLIC_KEY = ANSI_RED + NEXT_LINE
            + "Cannot Fetch the Public Key!" + NEXT_LINE + ANSI_RESET;
    public static final String ERROR_INVALID_TOKEN = ANSI_RED + NEXT_LINE
            + "Token is Invalid!!" + NEXT_LINE + ANSI_RESET;

    // Exception messages
    public static final String ERROR_NOT_INSTANTIATED = "This utility class cannot be instantiated";

    private static String createDivider(int length) {
        return new String(new char[length]).replace("\0", EQUALS);
    }
}

package com.alaric.auth.domain;

import static com.alaric.auth.util.Constants.AUTHORITY;
import static com.alaric.auth.util.Constants.CLIENT_ID;
import static com.alaric.auth.util.Constants.CLIENT_SECRET;
import static com.alaric.auth.util.Constants.COMMA;
import static com.alaric.auth.util.Constants.PASSWORD;
import static com.alaric.auth.util.Constants.SCOPE;
import static com.alaric.auth.util.Constants.USERNAME;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import lombok.Data;

@Data
public class Settings {

    private String authority;
    private String client_id;
    private String client_secret;
    private List<String> scope;
    private String username;
    private String password;

    public Settings(Properties properties) {
        authority = properties.getProperty(AUTHORITY);
        client_id = properties.getProperty(CLIENT_ID);
        client_secret = properties.getProperty(CLIENT_SECRET);
        scope = Arrays.asList(properties.getProperty(SCOPE).split(COMMA));
        username = properties.getProperty(USERNAME);
        password = properties.getProperty(PASSWORD);
    }

}

package com.alaric.auth.domain;

import static com.alaric.auth.util.Constants.DOT;
import static com.alaric.auth.util.Constants.DOT_ESCAPE;

import lombok.Data;

@Data
public class Token {

    private String accessToken;
    private String header;
    private String payload;
    private String signature;
    private String signingInfo;

    public Token(String token) {
        accessToken = token;
        final String[] tokenParts = accessToken.split(DOT_ESCAPE);
        header = tokenParts[0];
        payload = tokenParts[1];
        signature = tokenParts[2];
        signingInfo = String.join(DOT, header, payload);
    }

}

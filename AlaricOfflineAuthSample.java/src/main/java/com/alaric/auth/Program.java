package com.alaric.auth;

import static com.alaric.auth.util.Constants.ANSI_RESET;
import static com.alaric.auth.util.Constants.APP_PROPERTIES_PATH;
import static com.alaric.auth.util.Constants.CLIENT_SECRET_PATTERN;
import static com.alaric.auth.util.Constants.ERROR_INVALID_AUTHORITY_URL;
import static com.alaric.auth.util.Constants.ERROR_INVALID_CLIENT_SECRET;
import static com.alaric.auth.util.Constants.ERROR_INVALID_OPTION;
import static com.alaric.auth.util.Constants.ERROR_INVALID_PASSWORD;
import static com.alaric.auth.util.Constants.ERROR_INVALID_USERNAME;
import static com.alaric.auth.util.Constants.ERROR_MALFORMED_APP_PROPERTIES;
import static com.alaric.auth.util.Constants.ERROR_NOT_INSTANTIATED;
import static com.alaric.auth.util.Constants.ERROR_NULL_APP_PROPERTIES;
import static com.alaric.auth.util.Constants.ERROR_READING_APP_PROPERTIES_FILE;
import static com.alaric.auth.util.Constants.ERROR_TERMINATING_APPLICATION;
import static com.alaric.auth.util.Constants.INFO_FETCHED_DISCOVERY_DOCUMENT;
import static com.alaric.auth.util.Constants.INFO_FETCHING_DISCOVERY_DOCUMENT;
import static com.alaric.auth.util.Constants.INFO_LOADED_SETTINGS;
import static com.alaric.auth.util.Constants.INFO_LOADING_SETTINGS;
import static com.alaric.auth.util.Constants.INFO_PLEASE_PROVIDE_YOUR_CREDENTIALS;
import static com.alaric.auth.util.Constants.INFO_PLEASE_SELECT_OPTION;
import static com.alaric.auth.util.Constants.INFO_QUITTING;
import static com.alaric.auth.util.Constants.INTERACT_MENU;
import static com.alaric.auth.util.Constants.MENU_OPTION_PATTERN;
import static com.alaric.auth.util.Constants.OPTION_Q;
import static com.alaric.auth.util.Constants.OPTION_R;
import static com.alaric.auth.util.Constants.OPTION_U;
import static com.alaric.auth.util.Constants.OPTION_V;
import static com.alaric.auth.util.Constants.PROMPT_AUTHORITY;
import static com.alaric.auth.util.Constants.PROMPT_CLIENT_ID;
import static com.alaric.auth.util.Constants.PROMPT_CLIENT_SECRET;
import static com.alaric.auth.util.Constants.PROMPT_PASSWORD;
import static com.alaric.auth.util.Constants.PROMPT_SCOPES;
import static com.alaric.auth.util.Constants.PROMPT_USERNAME;
import static com.alaric.auth.util.Constants.SETTINGS_TEMPLATE;
import static com.alaric.auth.util.Constants.URL_PATTERN;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.alaric.auth.domain.Settings;
import com.alaric.auth.domain.Token;
import com.alaric.auth.util.TokenUtils;
import com.google.gson.JsonObject;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

public class Program {

    private Program() {
        throw new UnsupportedOperationException(ERROR_NOT_INSTANTIATED);
    }

    private static Token token;

    private static JsonObject discoJson;

    private static Settings settings;

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            loadSettings();

            if (!validateSettings()) {
                terminate();
                return;
            }

            promptSettings();

            if (StringUtils.isEmpty(settings.getUsername()) || StringUtils.isEmpty(settings.getPassword())) {
                if (!promptAndValidateCredentials(scanner)) {
                    terminate();
                    return;
                }
            }

            if (!checkDisco()) {
                terminate();
                return;
            }

            interact(scanner);

            System.out.println(INFO_QUITTING);
        }
    }

    private static boolean checkDisco() {
        final String authority = settings.getAuthority();
        System.out.println(INFO_FETCHING_DISCOVERY_DOCUMENT + authority + ANSI_RESET);
        discoJson = TokenUtils.retrieveDiscoJson(authority);
        if (discoJson == null) {
            return false;
        }
        System.out.println(INFO_FETCHED_DISCOVERY_DOCUMENT + authority + ANSI_RESET);
        return true;
    }

    private static void interact(Scanner scanner) {
        boolean quit = false;
        do {
            System.out.println(INTERACT_MENU);

            final String input = scanValue(scanner, INFO_PLEASE_SELECT_OPTION, ERROR_INVALID_OPTION);
            if (StringUtils.isEmpty(input)) {
                continue;
            }

            if (input.length() != 1 && !Pattern.matches(MENU_OPTION_PATTERN, input)) {
                System.out.println(ERROR_INVALID_OPTION);
                continue;
            }

            if (OPTION_R.equalsIgnoreCase(input)) {
                token = TokenUtils.retrieveToken(settings, discoJson);
                if (token != null) {
                    TokenUtils.promptToken(token);
                }
            } else if (OPTION_V.equalsIgnoreCase(input)) {
                TokenUtils.validateToken(token, settings, discoJson);
            } else if (OPTION_U.equalsIgnoreCase(input)) {
                promptAndValidateCredentials(scanner);
            } else if (OPTION_Q.equalsIgnoreCase(input)) {
                quit = true;
            } else {
                System.out.println(ERROR_INVALID_OPTION);
            }

        } while (!quit);
    }

    private static void terminate() {
        System.out.println(ERROR_TERMINATING_APPLICATION);
    }

    private static boolean promptAndValidateCredentials(Scanner scanner) {
        System.out.println(INFO_PLEASE_PROVIDE_YOUR_CREDENTIALS);

        final String username = scanValue(scanner, PROMPT_USERNAME, ERROR_INVALID_USERNAME);
        if (StringUtils.isEmpty(username)) {
            return false;
        }
        settings.setUsername(username);

        final String password = scanValue(scanner, PROMPT_PASSWORD, ERROR_INVALID_PASSWORD);
        if (StringUtils.isEmpty(password)) {
            return false;
        }
        settings.setPassword(password);

        return true;
    }

    private static String scanValue(Scanner scanner, String promptMessage, String errorMessage) {
        System.out.print(promptMessage);
        try {
            String value = scanner.nextLine();
            if (StringUtils.isEmpty(value)) {
                System.out.println(errorMessage);
                return null;
            }
            return value;
        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.println(errorMessage);
            return null;
        }
    }

    private static void promptSettings() {
        System.out.println(INFO_LOADED_SETTINGS);

        System.out.println(PROMPT_AUTHORITY + settings.getAuthority());
        System.out.println(PROMPT_CLIENT_ID + settings.getClient_id());
        System.out.println(PROMPT_CLIENT_SECRET + settings.getClient_secret());
        System.out.println(PROMPT_SCOPES + Arrays.toString(settings.getScope().toArray()));
    }

    private static boolean validateSettings() {
        if (settings == null) {
            System.out.println(ERROR_NULL_APP_PROPERTIES);
            return false;
        }

        final String authority = settings.getAuthority();
        final String clientId = settings.getClient_id();
        final String clientSecret = settings.getClient_secret();
        final List<String> scope = settings.getScope();
        if (StringUtils.isEmpty(authority) || StringUtils.isEmpty(clientId) || StringUtils.isEmpty(clientSecret)
                || CollectionUtils.isEmpty(scope) || scope.contains(null) || scope.contains(EMPTY)) {
            System.out.println(ERROR_MALFORMED_APP_PROPERTIES);
            System.out.println(SETTINGS_TEMPLATE);
            return false;
        }

        if (!Pattern.matches(URL_PATTERN, authority)) {
            System.out.println(ERROR_INVALID_AUTHORITY_URL);
            return false;
        }

        if (!Pattern.matches(CLIENT_SECRET_PATTERN, clientSecret)) {
            System.out.println(ERROR_INVALID_CLIENT_SECRET);
            return false;
        }

        return true;
    }

    private static void loadSettings() {
        final Properties properties = new Properties();
        System.out.println(INFO_LOADING_SETTINGS);
        try (final FileInputStream fileInputStream = new FileInputStream(APP_PROPERTIES_PATH)) {
            properties.load(fileInputStream);
            settings = new Settings(properties);
        } catch (IOException e) {
            System.out.println(ERROR_READING_APP_PROPERTIES_FILE);
        }
    }

}
